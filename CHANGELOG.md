# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]()

## [0.1.0]() - 2020-06-02

> WARNING: Initial Prototype which is not ready fpr productive use!

### Added

- Prototype calculates average, variance, and deviation.
- Sample values can be provided manually or by a file in CSV format.
